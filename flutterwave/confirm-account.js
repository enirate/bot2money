const flutterSecure = require('./flutter-secure');
const verifyAccountUrl = flutterSecure.verifyAccount;
// function to confirm user account
module.exports = function confirmAccount(freshToken, bankName, accountNumber, axios) {
    let bankCode;
    let cleanAccountNumber = accountNumber.trim();
    switch (bankName) {
        case 'Access Bank':
            bankCode = '044';
            break;
        case 'GTBank':
            bankCode = '058';
            break;
    
        default:
            '044';
    }

    let conConfig = {
        headers:{
            'Content-Type':'application/json',
            'Authorization': freshToken
        }
    };

    let body = {
        account_number: cleanAccountNumber,
        bank_code: bankCode
    }

    axios.post(verifyAccountUrl, body, conConfig)
    .then(response => {
        console.log(response);
    })
    .catch(err => {
        console.log(err);
    })




};