const axios = require('axios');
const flutterSecure = require('./flutter-secure');
const verifyUrl = flutterSecure.verifyMerchant;
const apiKey = flutterSecure.apiKey;
const secretPass = flutterSecure.secret;
 
// get token for transactions
module.exports = {
    returnToken: function getToken(verifyMerchant, apiKey, secretPass, axios, callback){
    let returned;
    // set header
    let config =
     {
        headers:{'Content-Type':'application/json'}
    };

    //return token or catch error
    axios.post(verifyUrl, {apiKey: apiKey, secret: secretPass}, config)
    .then((response) =>{
        // return 'response.data.token';
        // returned = 'response.data.token';
        // console.log(returned);
        callback(response.data.token);
    })
    .catch(err => {
        // return err
        console.log(err);
    })
}
}

// getToken(verifyUrl, apiKey, secretPass, axios);

