module.exports = function(bankCode) {
    let finalValue;
    switch (bankCode) {
        case 'Access Bank':
            finalValue = '044';
            break;

        case 'GTBank':
            finalValue = '058';
            break;
    
        default:
            '044';
    }
    return finalValue;
}