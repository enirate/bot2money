// this is the module that controls persistent menu
module.exports = (bot) => {
    botMenu = [
        { type: 'postback', title: 'Help', payload: 'GET_HELP' },
        { type: 'postback', title: 'Transfer Fund', payload: 'TRANSFER' }
    ]
    bot.setPersistentMenu(botMenu);
}