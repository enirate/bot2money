// this is the module that controls helpblock
module.exports = (bot) => {
    let helpText = `To transfer money reply with any of these keywords 'Transfer money', 'send money' or 'Transfer'`;    
    bot.on('postback:GET_HELP', (payload, chat) => {
    chat.say(helpText)
});

    bot.hear([/help/i], (payload, chat) => {
        chat.say(helpText);
    });
}