module.exports = (bot) => {
    let textTransfer = "I'm about to start the transfer process, are you ready?"
    let qrBtn = ['I\'m ready', 'Not now']
    bot.on('postback:TRANSFER', (payload, chat) => {
        chat.sendTextMessage(textTransfer, qrBtn);
    });
}