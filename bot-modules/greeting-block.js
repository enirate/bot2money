module.exports = (bot) => {
    bot.on('postback:Greeting', (payload, chat) =>{
    chat.getUserProfile().then((user) => {
        chat.say(
            {
                text: `Hi, ${user.first_name}! I help Nigerians abroad to send money back home without stress. Are you ready to transfer money?`,
                quickReplies: ['I\'m ready', 'Not now']
            });
    })
})
};