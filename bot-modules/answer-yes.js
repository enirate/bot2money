const axios = require('axios');

// parameters from flutterwave module
const flutterwave = require('../flutterwave/flutter-secure');
const verifyMerchantUrl = flutterwave.verifyMerchant;
const verifyAccountUrl = flutterwave.verifyAccount;
const apiKey = flutterwave.apiKey;
const secretDiscreet = flutterwave.secret;
const normalHeader = flutterwave.normalHeader;
const switchBankToCode = require('./switch-code');

module.exports = (bot) => {
    // functions that handles conversation

    // Ask customer the name of receipient
        const askReceipient = (convo) => {
            convo.ask('What is the account number of the person you want to send the money to?', (payload, convo) =>{
                const text = payload.message.text;
                if(isNaN(text))
                {
                    convo.say('You must enter a number please?');
                    askReceipient(convo);
                }
                else
                {
                    convo.set('receipient', text)
                    askAmount(convo);
                }
            })
        }

            
        // ask about the amount
        const askAmount = (convo) => {
            convo.ask('How much do you want to send in US dollars($)?', (payload, chat) => {
                const text = payload.message.text;
                if(isNaN(text))
                { 
                    convo.say('You must enter a number please?');
                    askAmount(convo);
                }
                else
                {
                    convo.set('amount', text);
                    askReceipientBank(convo);
                }               
                           
            });
        }

        // get the receipient bank
        const askReceipientBank = (convo) => {
            let token;
            let bankCode;
            let amountToSend = convo.get('amount');
            let deduct = Math.floor((amountToSend / 100) * 3);
            // addition signs turn values to integer
            let totalCost = +amountToSend + +deduct;
            let cleanAccountNumber = convo.get('receipient').trim();

            // ask about transaction details
            convo.ask({
                text: 'Which bank is the person using?',
                quickReplies: ['Access Bank', 'GTBank', 'UBA', 'FCMB', 'Zenith Bank', 'First Bank', 'EcoBank']
            }, (payload, chat) => {
                const text = payload.message.text;
                convo.set('bank', text);

                // bank name will be matched to code
                bankCode = switchBankToCode(convo.get('bank'));                
                convo.say(`Please wait while I prepare the transfer details`, {typing: 3000})
                .then(() => {
                    axios.post(verifyMerchantUrl, {apiKey: apiKey, secret:secretDiscreet}, normalHeader)
                    .then((response) => {

                        // we assign token to variable and acts on it
                        token = response.data.token;
                        if(token)
                        {
                            // header configuration for account verification
                            let resConfig = { headers:{'Content-Type':'application/json','Authorization': token}};

                            // payload that contains user defined parameters i.e account & bankcode
                            let bodyPayload = {account_number: cleanAccountNumber, bank_code: bankCode}
                            axios.post(verifyAccountUrl, bodyPayload, resConfig)
                            .then((response) => {
                                let recName = response.data.data.account_name;
                                convo.ask((convo) => {//convo start
                                // detail text to send
                                let detailText = `Transfer Summary:
                                Receipient: ${recName}
                                Amount: $${convo.get('amount')}
                                Charge (3%): $${deduct}
                                Total: $${totalCost}`;
                                // cofirmation button
                                const buttons = [
                                    { type: 'postback', title: 'Confirm Transaction', payload: 'TRANSACT_YES' },
                                    { type: 'postback', title: 'Cancel Transaction', payload: 'TRANSACT_NO' }
                                ];
                                convo.sendButtonTemplate(detailText, buttons);
                            }, 
                            (payload, convo, data) => {
                                const text = payload.message.text;
                                // convo.set('gender', text);
                                // convo.say(`Great, you are a ${text}`).then(() => askAge(convo));
                            }, 
                            [
                                {
                                    event: 'postback:TRANSACT_NO',
                                    callback: (payload, convo) => {
                                        convo.say('OK, I\'ll cancel the transaction now').then(() => cancelTransaction(convo));
                                        }
                                },
                                {
                                    event: 'postback:TRANSACT_YES',
                                    callback: (payload, convo) => {
                                        convo.say('Ok. I\'ll complete the transaction').then(() => confirmTransaction(convo));
                                        }
                                },
                            ])//convo.say end
                            })
                            .catch((err) => {
                                convo.say('Invalid account number, reply with \'Transfer\' to try again.')
                                console.log(err);
                                convo.end();
                            })
                        }
                    })
                    .catch((err) => convo.say('cannot get name'))
                })
            });
        }


    // confirm transaction convo
        const confirmTransaction = (convo) => {
            convo.say(`That's great! This should take customers to pop up`)
            convo.end();                                
        };
    // confirm transaction convo
    

    // end transaction
        const cancelTransaction = (convo) => {
                convo.say(`Ok I have cancelled the transaction. I didn't store any information, just so you know.`, {typing: true
                });            
                convo.end();                
            };
    // end transaction

     //bot listens to transfer keywords and initiate a converstion
    bot.hear(['I\'m ready', /transfer/i, /send money/i, /transfer fund/i, /send fund/i], (payload, chat) =>{
        chat.conversation((convo) => {
            askReceipient(convo);
        });


   
 
   
});

}