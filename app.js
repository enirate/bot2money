'use strict';

// Node modules 
const express = require("express");
const secured = require('./secured');
const BootBot = require('bootbot');
const axios = require('axios');

// flutterwave related
const flutterSecure = require('./flutterwave/flutter-secure');
const verifyUrl = flutterSecure.verifyMerchant;
const apiKey = flutterSecure.apiKey;
const secretPass = flutterSecure.secret;

// import chatbot modules
const greetingBlock = require('./bot-modules/greeting-block')
const persistentMenu = require('./bot-modules/persistent-menu');
const helpBlock = require('./bot-modules/help-block');
const answerNo = require('./bot-modules/answer-no');
const answerYes = require('./bot-modules/answer-yes');
const persistentTransfer = require('./bot-modules/persistent-transfer');
const getToken = require('./flutterwave/get-token');


// enivironment port or 3000
const port = process.env.PORT || 3000;

// creating an instance of Bootbot for our bot
const bot = new BootBot({
  accessToken: secured.pageToken,
  verifyToken: secured.verificationToken,
  appSecret: secured.appSecret
});

// implement chatbot modules
bot.module(greetingBlock);
bot.module(persistentMenu);
bot.module(persistentTransfer);
bot.module(answerYes);
bot.module(answerNo);
bot.module(helpBlock);


// start express server
bot.start(port);

// production webhook.
// https://bot2money.herokuapp.com/webhook